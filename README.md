# INDES Weekly Newsletter

Die INDES Graz Willkommensmail 1.0

Letzte Update: 21.11.2018

## Benötigte Werkzeuge
* Editor (z.B. Sublime -> https://www.sublimetext.com/3 )
* Browser (Chrome funktioniert zuverlässig)


## Ablauf
* welcome.html herunterladen
* welcome.html bearbeiten
* Datei in Browser öffnen
* Im Browser alles markieren und kopieren
* In Webclient einfügen und verschicken
* Glücklich sein!

## Probleme
* Darstellung der welcome.html funktioniert am besten via Chrome
* Webmail funktioniert am besten mit Firefox

Wenn es Überarbeitungen an der weekly.html Vorlage gibt, einfach die weekly.html erneut herunterladen.